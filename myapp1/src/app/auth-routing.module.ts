import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddbookComponent } from './components/books/AddBook/Addbook.component';
import { BookComponent } from './components/books/Booklist/book.component';
import { DataComponent } from './components/data/data.component';
import { AddAuthorComponent } from './components/employee/Add/AddAuthor.component';
import { AuthorlistComponent } from './components/employee/Authorlist/Authorlist.component';
import { LoginComponent } from './components/login/logins/login.component';
import { RegisterComponent } from './components/login/register/register.component';
import { PageNotFoundComponent } from './components/PageNotFoundComponent.component';
import { AuthorGuard } from './_helpers/author.guard';

const routes: Routes = [
  { path: '', redirectTo: '/DataComponent', pathMatch: 'full' },
  // { path: 'register', component: RegisterComponent },
  // { path: 'login', component: LoginComponent },
  // { path: 'Addbook', component: AddbookComponent, },
  // { path: 'Book', component: BookComponent },
  // { path: 'Authorlist', component: AuthorlistComponent },
  // { path: 'AddAuthor', component: AddAuthorComponent, canDeactivate: [AuthorGuard] },  
  { path: 'DataComponent', component: DataComponent },
  //{ path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AuthRoutingModule { }
