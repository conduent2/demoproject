export class User {
    id: number;
    name: string;
    email: string;
    password: string;
    token: string;
}
export class JwtResponse {    
        id: number;
        UserName: string;
        Email: string;
        token: string ="" ;
        access_token : string;
        expires_in: Date;
        message: string;
        IsAuthenticated: boolean;

    
}