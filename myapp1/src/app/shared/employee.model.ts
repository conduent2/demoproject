export class Author {
    public AuthorId: number;
    public FirstName: string;
    public LastName: string;
    //public Identification: string;
    //public Birthdate: Date;
    public gender: string;
    public Book: Books[];
    public Biography: AuthorBiography[]
}

export class Books {
    public BookId: number;
    public Title: string;
    public Author: Author[];
}

export class AuthorBiography {

}
