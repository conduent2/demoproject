export class Loginrequestmodel {
    public email: string;
    public password: string;
}

export class Loginreturnmodel {
    public Message: string;
    public IsAuthenticated: boolean;
    public UserName: string;
    public Email: string;
    public Roles: String[];
    public Token: string;
}
