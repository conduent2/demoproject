import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Author } from './employee.model';
import { Books } from './employee.model';
import { from, Observable, timer } from 'rxjs';
import { concatMap, delay } from 'rxjs/operators';
import { of } from 'rxjs'
import { AuthenticationService } from '../_helpers/authentication.service';

@Injectable({
  providedIn: 'root',

})
export class AuthorService {

  AuthorData = new Author();
  AuthorList: Author[];
  readonly rootUrl = 'https://localhost:44356/api/';
  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {

    var currentUser = this.authenticationService.currentUserValue;
  }


  async refreshList() {
    let httpOptions = new HttpHeaders()
      .set('Authorization', 'Bearer ' + '');

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'No-Auth': 'True' });
    var authorlist = await this.http.get(this.rootUrl + 'authors', { headers: reqHeader })
      .toPromise().then(res => this.AuthorList = res as Author[]);
    console.log(authorlist);
    console.log(this.AuthorList);
    return this.AuthorList;
  }

  async AuthorAdd(Author) {
    debugger;
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'No-Auth': 'True' });
    await this.http.post(this.rootUrl + 'authors', Author, { headers: reqHeader });

  }

}
