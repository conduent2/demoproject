import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/shared/auth.model';
import { JwtResponse } from 'src/app/shared/auth.model';
import { map, tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  AUTH_SERVER = "https://localhost:44356/api";
  authSubject = new BehaviorSubject(false);
  public jwtres: JwtResponse;
  constructor(private httpClient: HttpClient,) { }

  register(user: User): Observable<JwtResponse> {
    this.httpClient.post<JwtResponse>(`${this.AUTH_SERVER}/user/register`, user).subscribe(
      res => {
        console.log(res);
        this.jwtres = res as JwtResponse;
        console.log(this.jwtres);
      }
    )
    return this.httpClient.post<JwtResponse>(`${this.AUTH_SERVER}/user/register`, user).pipe(
      tap((res: JwtResponse) => {
        debugger;
        console.log(res);
        if (res != null) {
          debugger;
          localStorage.set("ACCESS_TOKEN", res.access_token);
          localStorage.set("EXPIRES_IN", res.expires_in);
          this.authSubject.next(true);
        }
      })
    );
  }

  async signIn(user: User): Promise<Observable<JwtResponse>> {
    debugger;
    return await this.httpClient.post<JwtResponse>(`${this.AUTH_SERVER}/user/posttoken`, user).pipe(
      map((res: JwtResponse) => {
        if (res != null) {
          debugger;
          //this.jwtres.token = '';
          this.jwtres = res as JwtResponse;
          console.log(this.jwtres);          
          var dd  = JSON.stringify(res.token).toString();
          localStorage.setItem("ACCESS_TOKEN", JSON.stringify(res.token).toString());
          localStorage.setItem("EXPIRES_IN", this.jwtres.expires_in.toString());
          this.authSubject.next(true);
          return this.jwtres;
        }
      }),
    );
  }

  signOut() {
    debugger;
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("EXPIRES_IN");
    this.authSubject.next(false);
  }

  isAuthenticated() {
    debugger;
    return this.authSubject.asObservable();
  }
}
