import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Author, Books } from 'src/app/shared/employee.model';
import { AuthorService } from 'src/app/shared/authorservice.service';

@Component({
  selector: 'add-Author',
  templateUrl: './AddAuthor.component.html'
})
export class AddAuthorComponent implements OnInit {
  public authorlist: Author[] = [];
  public author = new Author();
  public Books = new Books();
  public Booklist: Books[] = [];
  @Output() notify: EventEmitter<any> = new EventEmitter<any>();  
  @ViewChild('author') public createAuthorForm: NgForm;
  constructor(private service: AuthorService) {
  }

  ngOnInit() {
  }

  saveEmployee(authorform?: NgForm) {
    debugger;
    console.log(authorform);
    this.Booklist.push({
      'Title': authorform.form.controls["Title"].value,
      'BookId': null, 'Author': null
    });
    this.author.FirstName = authorform.form.controls["FirstName"].value;
    this.author.LastName = authorform.form.controls["LastName"].value;
    this.author.Book = this.Booklist;
    this.notify.emit(this.author);
  }

  handleNotifyBook(Book?: NgForm) {
  }
}
