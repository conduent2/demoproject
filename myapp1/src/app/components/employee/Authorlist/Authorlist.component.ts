import { Component, Input, OnInit } from '@angular/core';
import { Author, Books } from 'src/app/shared/employee.model';
import { AuthorService } from 'src/app/shared/authorservice.service';

@Component({
  selector: 'app-Authorlist',
  templateUrl: './Authorlist.component.html',
  styleUrls: ['./Authorlist.component.css']
})
export class AuthorlistComponent implements OnInit {

  public authorlist: Author[];
  public objAuthor = new Author();
  public selectedbook: Books[];
  public show: boolean = false;
  addbutton: string = "";
  constructor(private service: AuthorService) {
    debugger;
    var storage = localStorage.getItem('currentUser');
    alert(storage);
    this.addbutton = "Add";
  }

  ngOnInit() {
    this.loadAuthor();
  }
  loadAuthor() {
    debugger;
    this.service.refreshList().then(res => {
      this.authorlist = res as Author[];
      this.selectedbook = null;
      console.log(this.authorlist);
    }, err => { console.log(err); });
  }
  onSave(Author) {
    debugger;
    this.service.AuthorAdd(Author).then(res => { this.loadAuthor(); })
  }
  onBook(book) {
    if (book != null) {
      this.selectedbook = book;
    }
    else {
      this.selectedbook = null;
    }
  }
  onEdit() {
    alert("edit");
  }
  onDelete() {
  }
  onAdd() {
    this.show = !this.show;
    if (this.show == true) {
      this.addbutton = "Add";
    }
    else {
      this.addbutton = "Remove";
    }
  }
  public dataFromChild: any;
  handleNotify(eventData: Author) {
    debugger;
    this.dataFromChild = eventData;
    this.onSave(Author);
  }
  handleNotifyBook(eventData: Author) {
    debugger;
    this.dataFromChild = eventData;
  }

}
