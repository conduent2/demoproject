import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor( private authservice : AuthService, private router : Router) { }

  ngOnInit(): void {    
  }

  register(form) {
    console.log(form.value);
    this.authservice.register(form.value).subscribe((res) => {
      this.router.navigateByUrl('login');
    });
  }
}
