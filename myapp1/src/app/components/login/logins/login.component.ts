import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { AuthenticationService } from 'src/app/_helpers/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  constructor(private authService: AuthService, private router: Router,
    private authser: AuthenticationService, private route: ActivatedRoute,
    private authenticationService: AuthenticationService) {
    if (this.authenticationService.currentUserValue) {
      debugger;
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
  }
  async login(form) {
    console.log(form.value);
    await this.authser.login(form.value).subscribe((res) => {
      console.log("Logged in!" + res);
     // this.router.navigateByUrl('/Authorlist');
      this.router.navigate(['/Authorlist']);
    });
  }
  LogOut() {
    alert("ddd");
    this.authService.signOut();
  }
}