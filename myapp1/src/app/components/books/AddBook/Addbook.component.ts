import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Author, Books } from 'src/app/shared/employee.model';
import { AuthorService } from 'src/app/shared/authorservice.service';

@Component({
  selector: 'add-book',
  templateUrl: './Addbook.component.html'
})
export class AddbookComponent implements OnInit {

  public Booklist: Books[];
  public Books = new Books();
  @Output() notify: EventEmitter<Books> = new EventEmitter<Books>();
  constructor(private service: AuthorService) {
  }
  ngOnInit() {
  }
  saveEmployee(booksform?: NgForm) {
    debugger;
    console.log(booksform);
    this.Books.Title = booksform.form.controls["Title"].value;
    this.notify.emit(this.Books);
  }

}
