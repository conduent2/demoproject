import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Author, Books } from 'src/app/shared/employee.model';
import { AuthorService } from 'src/app/shared/authorservice.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html'
})
export class BookComponent implements OnInit {

  //public booklist: Books[];
  //public objBooks = new Books();
  @Input() book: Books[];
  constructor(private service: AuthorService) {
    console.log(this.book);
  }


  ngOnInit() {
    this.loadBook();
  }
  loadBook() {

  }
}
