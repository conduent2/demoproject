import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from 'src/app/auth-routing.module';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material/material.module';

import { AppComponent } from './app.component';
import { AuthorlistComponent } from './components/employee/Authorlist/Authorlist.component';
import { AuthorService } from './shared/authorservice.service';
import { BookComponent } from './components/books/Booklist/book.component';
import { AddAuthorComponent } from './components/employee/Add/AddAuthor.component';
import { AddbookComponent } from './components/books/AddBook/Addbook.component';
import { LoginComponent } from './components/login/logins/login.component';
import { RegisterComponent } from './components/login/register/register.component';
import { AuthService } from './shared/auth.service';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { PageNotFoundComponent } from './components/PageNotFoundComponent.component';
import { AuthorGuard } from './_helpers/author.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingpageComponent } from './components/landingpage/landingpage.component';
import { DataComponent} from './components/data/data.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthorlistComponent, BookComponent, AddAuthorComponent, AddbookComponent,
    LoginComponent, RegisterComponent, PageNotFoundComponent, LandingpageComponent,DataComponent
  ],
  imports: [
    BrowserModule,MaterialModule, RouterModule, HttpClientModule, AuthRoutingModule,
     FormsModule, ReactiveFormsModule, BrowserAnimationsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthorService, AuthService, AuthorGuard
  ],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
