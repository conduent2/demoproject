import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { AddAuthorComponent } from '../components/employee/Add/AddAuthor.component';

@Injectable()
export class AuthorGuard implements CanDeactivate<AddAuthorComponent> {

    constructor() { }

    canDeactivate(component: AddAuthorComponent): boolean {
        if (component.createAuthorForm.dirty) {
            return confirm('Are you sure you want to discard your changes?');
        }

        return true;
    }
}
